# README #

To run this ansible project, update the IP address / hostname of the server under [remoteserver] in the remoteserver file as appropriate.

Then run the following command to execute:

```
#!bash

ansible-playbook tpdevops.yml -i remoteserver
```